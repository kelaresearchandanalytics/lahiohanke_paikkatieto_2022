# Skriptissä käydään läpi Syke:n datoja.
# Koodit peräisin: http://markuskainu.fi/2021/01/katsaus-ymparistokeskuksen-aluejakoaineistoihin/

source("./scripts/global.R")
fldr <- "./datasets/"
fs::dir_create(fldr)

geopkg_file <- "./datasets/syke_3067.gpkg"
file.remove(geopkg_file)

# kartanpiirtofunktio

#' Suomen ympäristökeskuksella on laaja kokoelma avoimia (`CC-BY 4.0`)
#' paikkatietoaineistoja [ladattavat paikkatietoaineistot](https://www.syke.fi/fi-FI/Avoin_tieto/Paikkatietoaineistot/Ladattavat_paikkatietoaineistot)-sivuillaan.
#' Valtaosa aineistoista käsittelee ns. luonnonympäristöä, mutta kokoelmassa on paljon myös rakennettuun ympäristöön liittyvää.
#' Tässä analyysissä kartoitetaan kokoelman datoja erään lähiöiden elinoloja tarkastelevan tutkimusprojektin tarpeisiin.

#' Kävin kokoelman läpi aakkosjärjestyksessä ja poimin lähempään tarkasteluun ne,
#' jotka jollain tapaa käsittivät *hallinnollisia rajoja*/*aluejakoja*. Tarkemmassa käsittelyssä on aina
#' aineiston uusin versio ja mikäli mahdollista, data on rajattu Helsinkiin.
#' Yhdessä datassa (zipattu paketti) voi olla useampi kerros, jolloin kerrokset ovat omina karttoinaan.
#' Vuorovaikutteisessa kartassa täyttö on datan ensimmäisen sarakkeen mukaan ja hoveroimalla kartan päällä, näet kaikki alkuperäisen datan muuttujat.

#' # Asuinalueet 2019

#' Asuinalueet ([metadata](https://ckan.ymparisto.fi/dataset/%7B0B77FA8C-7C42-4783-AA5C-868152E32BE0%7D))-datasta löytyvät seuraavat datat.

#+
dir_create("~/local_data/lahiohanke_paikkatietoaineistot/raw_data/")
dir_create("~/local_data/lahiohanke_paikkatietoaineistot/raw_data/syke/asuinalueet")
download.file(url = "https://wwwd3.ymparisto.fi/d3/gis_data/spesific/asuinalueet2019.zip",
              "~/local_data/lahiohanke_paikkatietoaineistot/raw_data/syke/asuinalueet/file.zip", mode = "wb")
unzip("~/local_data/lahiohanke_paikkatietoaineistot/raw_data/syke/asuinalueet/file.zip", 
      exdir = "~/local_data/lahiohanke_paikkatietoaineistot/raw_data/syke/asuinalueet/")
dir_ls("~/local_data/lahiohanke_paikkatietoaineistot/raw_data/syke/asuinalueet/", regexp = "shp$")

#' ## HarvaPientaloAlue19.shp

dir_create("./datasets/")

#+
nc <- st_read("~/local_data/lahiohanke_paikkatietoaineistot/raw_data/syke/asuinalueet/HarvaPientaloAlue19.shp")
nc_3067 <- nc %>% st_transform(crs = st_crs(crs_3067))
sf::st_write(obj = nc_3067, dsn = geopkg_file, layer = "HarvaPientaloAlue19")

#' ## PientaloAlue19.shp

#+
nc <- st_read("~/local_data/lahiohanke_paikkatietoaineistot/raw_data/syke/asuinalueet/PientaloAlue19.shp")
nc_3067 <- nc %>% st_transform(crs = st_crs(crs_3067))
sf::st_write(obj = nc_3067, dsn = geopkg_file, layer = "PientaloAlue19")

#' ## KerrostaloAlue19.shp

#+
nc <- st_read("~/local_data/lahiohanke_paikkatietoaineistot/raw_data/syke/asuinalueet/KerrostaloAlue19.shp")
nc_3067 <- nc %>% st_transform(crs = st_crs(crs_3067))
sf::st_write(obj = nc_3067, dsn = geopkg_file, layer = "KerrostaloAlue19")

#' # Asemakaavoitettu alue 31.12.2019

#+
dir_create("~/local_data/lahiohanke_paikkatietoaineistot/raw_data/syke/asemakaavoitettu")
download.file(url = "https://wwwd3.ymparisto.fi/d3/gis_data/spesific/asemakaavoitettualue2019.zip",
              "~/local_data/lahiohanke_paikkatietoaineistot/raw_data/syke/asemakaavoitettu/file.zip", mode = "wb")
unzip("~/local_data/lahiohanke_paikkatietoaineistot/raw_data/syke/asemakaavoitettu/file.zip", 
      exdir = "~/local_data/lahiohanke_paikkatietoaineistot/raw_data/syke/asemakaavoitettu/")
dir_ls("~/local_data/lahiohanke_paikkatietoaineistot/raw_data/syke/asemakaavoitettu/", regexp = "shp$")

#+
nc <- st_read("~/local_data/lahiohanke_paikkatietoaineistot/raw_data/syke/asemakaavoitettu/AsKaavoitettuAlue2019.shp")
nc_3067 <- nc %>% st_transform(crs = st_crs(crs_3067))
sf::st_write(obj = nc_3067, dsn = geopkg_file, layer = "AsKaavoitettuAlue2019")

#' # Harva ja tiheä taajama-alue 2019 (YKR)

#+
dir_create("~/local_data/lahiohanke_paikkatietoaineistot/raw_data/syke/harvatihea")
download.file(url = "https://wwwd3.ymparisto.fi/d3/gis_data/spesific/harvatihea2019.zip",
              "~/local_data/lahiohanke_paikkatietoaineistot/raw_data/syke/harvatihea/file.zip", mode = "wb")
unzip("~/local_data/lahiohanke_paikkatietoaineistot/raw_data/syke/harvatihea/file.zip", exdir = "~/local_data/lahiohanke_paikkatietoaineistot/raw_data/syke/harvatihea/")
dir_ls("~/local_data/lahiohanke_paikkatietoaineistot/raw_data/syke/harvatihea/", 
       regexp = "shp$")


#+
nc <- st_read("~/local_data/lahiohanke_paikkatietoaineistot/raw_data/syke/harvatihea/HarvaTiheaTaajama2019.shp")
nc_3067 <- nc %>% st_transform(crs = st_crs(crs_3067))
sf::st_write(obj = nc_3067, dsn = geopkg_file, layer = "HarvaTiheaTaajama2019")

#' # Kaupunki-maaseutuluokitus 2018 (YKR)

#+
dir_create("~/local_data/lahiohanke_paikkatietoaineistot/raw_data/syke/kaupunkimaaseutu")
download.file(url = "https://wwwd3.ymparisto.fi/d3/gis_data/spesific/YKRKaupunkiMaaseutuLuokitus2018.zip",
              "~/local_data/lahiohanke_paikkatietoaineistot/raw_data/syke/kaupunkimaaseutu/file.zip", mode = "wb")
unzip("~/local_data/lahiohanke_paikkatietoaineistot/raw_data/syke/kaupunkimaaseutu/file.zip", exdir = "~/local_data/lahiohanke_paikkatietoaineistot/raw_data/syke/kaupunkimaaseutu/")
dir_ls("~/local_data/lahiohanke_paikkatietoaineistot/raw_data/syke/kaupunkimaaseutu/", regexp = "shp$")


#+
nc <- st_read("~/local_data/lahiohanke_paikkatietoaineistot/raw_data/syke/kaupunkimaaseutu/YKRKaupunkiMaaseutuLuokitus2018.shp")
nc_3067 <- nc %>% st_transform(crs = st_crs(crs_3067))
sf::st_write(obj = nc_3067, dsn = geopkg_file, layer = "YKRKaupunkiMaaseutuLuokitus2018")

#' # Kaupunkiseudut 2019 (YKR)

#+
dir_create("~/local_data/lahiohanke_paikkatietoaineistot/raw_data/syke/kaupunkiseutu")
download.file(url = "https://wwwd3.ymparisto.fi/d3/gis_data/spesific/YKRKaupunkiseutu2019.zip",
              "~/local_data/lahiohanke_paikkatietoaineistot/raw_data/syke/kaupunkiseutu/file.zip", mode = "wb")
unzip("~/local_data/lahiohanke_paikkatietoaineistot/raw_data/syke/kaupunkiseutu/file.zip", exdir = "~/local_data/lahiohanke_paikkatietoaineistot/raw_data/syke/kaupunkiseutu/")
dir_ls("~/local_data/lahiohanke_paikkatietoaineistot/raw_data/syke/kaupunkiseutu/", regexp = "shp$")


#' ## YKRKeskustaajama19.shp

#+
nc <- st_read("~/local_data/lahiohanke_paikkatietoaineistot/raw_data/syke/kaupunkiseutu/YKRKeskustaajama19.shp")
nc_3067 <- nc %>% st_transform(crs = st_crs(crs_3067))
sf::st_write(obj = nc_3067, dsn = geopkg_file, layer = "YKRKeskustaajama19")

#' ## YKRLahitaajama19.shp

#+
nc <- st_read("~/local_data/lahiohanke_paikkatietoaineistot/raw_data/syke/kaupunkiseutu/YKRLahitaajama19.shp")
nc_3067 <- nc %>% st_transform(crs = st_crs(crs_3067))
sf::st_write(obj = nc_3067, dsn = geopkg_file, layer = "YKRLahitaajama19")


#' ## YKRLievealue19.shp

#+
nc <- st_read("~/local_data/lahiohanke_paikkatietoaineistot/raw_data/syke/kaupunkiseutu/YKRLievealue19.shp")
nc_3067 <- nc %>% st_transform(crs = st_crs(crs_3067))
sf::st_write(obj = nc_3067, dsn = geopkg_file, layer = "YKRLievealue19")

#' # Taajamat 2019 (YKR)

#+
dir_create("~/local_data/lahiohanke_paikkatietoaineistot/raw_data/syke/taajamat")
download.file(url = "https://wwwd3.ymparisto.fi/d3/gis_data/spesific/YKRTaajamat2019.zip",
              "~/local_data/lahiohanke_paikkatietoaineistot/raw_data/syke/taajamat/file.zip", mode = "wb")
unzip("~/local_data/lahiohanke_paikkatietoaineistot/raw_data/syke/taajamat/file.zip", exdir = "~/local_data/lahiohanke_paikkatietoaineistot/raw_data/syke/taajamat/")
dir_ls("~/local_data/lahiohanke_paikkatietoaineistot/raw_data/syke/taajamat/", regexp = "shp$")

#+
nc <- st_read("~/local_data/lahiohanke_paikkatietoaineistot/raw_data/syke/taajamat/YKRTaajama19.shp")
nc_3067 <- nc %>% st_transform(crs = st_crs(crs_3067))
sf::st_write(obj = nc_3067, dsn = geopkg_file, layer = "YKRTaajama19")

#' # Yhdyskuntarakenteen aluejaot 2019 (YKR)

#+
dir_create("~/local_data/lahiohanke_paikkatietoaineistot/raw_data/syke/yhdyskuntarakenne")
download.file(url = "https://wwwd3.ymparisto.fi/d3/gis_data/spesific/YKRAsutus2019.zip",
              "~/local_data/lahiohanke_paikkatietoaineistot/raw_data/syke/yhdyskuntarakenne/file.zip", mode = "wb")
unzip("~/local_data/lahiohanke_paikkatietoaineistot/raw_data/syke/yhdyskuntarakenne/file.zip", 
      exdir = "~/local_data/lahiohanke_paikkatietoaineistot/raw_data/syke/yhdyskuntarakenne/")
dir_ls("~/local_data/lahiohanke_paikkatietoaineistot/raw_data/syke/yhdyskuntarakenne/", regexp = "shp$")


#' ## YKRKyla19.shp

#+
nc <- st_read("~/local_data/lahiohanke_paikkatietoaineistot/raw_data/syke/yhdyskuntarakenne/YKRKyla19.shp")
nc_3067 <- nc %>% st_transform(crs = st_crs(crs_3067))
sf::st_write(obj = nc_3067, dsn = geopkg_file, layer = "YKRKyla19")

#' ## YKRMaaseutu19.shp

#+
nc <- st_read("~/local_data/lahiohanke_paikkatietoaineistot/raw_data/syke/yhdyskuntarakenne/YKRMaaseutu19.shp")
nc_3067 <- nc %>% st_transform(crs = st_crs(crs_3067))
sf::st_write(obj = nc_3067, dsn = geopkg_file, layer = "YKRMaaseutu19")


#' ## YKRPienkyla19.shp

#+
nc <- st_read("~/local_data/lahiohanke_paikkatietoaineistot/raw_data/syke/yhdyskuntarakenne/YKRPienkyla19.shp")
nc_3067 <- nc %>% st_transform(crs = st_crs(crs_3067))
sf::st_write(obj = nc_3067, dsn = geopkg_file, layer = "YKRPienkyla19")

#' ## YKRTaajama19.shp

#+
# löytyy jo kansiosta "taajamat"
# nc <- st_read("~/local_data/lahiohanke_paikkatietoaineistot/raw_data/syke/yhdyskuntarakenne/YKRTaajama19.shp")
# nc_3067 <- nc %>% st_transform(crs = st_crs(crs_3067))
# saveRDS(nc_3067, "YKRTaajama19_3067.RDS")
# sf::st_write(obj = nc_3067, dsn = geopkg_file, layer = "YKRTaajama19")

#' # Yhdyskuntarakenteen vyöhykkeet 2017 (YKR)

#+
dir_create("~/local_data/lahiohanke_paikkatietoaineistot/raw_data/syke/yhdyskuntarakennevyohyke")
download.file(url = "https://wwwd3.ymparisto.fi/d3/gis_data/spesific/YKRVyohykkeet2017.zip",
              "~/local_data/lahiohanke_paikkatietoaineistot/raw_data/syke/yhdyskuntarakennevyohyke/file.zip", mode = "wb")
unzip("~/local_data/lahiohanke_paikkatietoaineistot/raw_data/syke/yhdyskuntarakennevyohyke/file.zip", 
      exdir = "~/local_data/lahiohanke_paikkatietoaineistot/raw_data/syke/yhdyskuntarakennevyohyke/")
dir_ls("~/local_data/lahiohanke_paikkatietoaineistot/raw_data/syke/yhdyskuntarakennevyohyke/", regexp = "shp$")

#+
nc <- st_read("~/local_data/lahiohanke_paikkatietoaineistot/raw_data/syke/yhdyskuntarakennevyohyke/YKRVyohykkeet2017.shp")
nc_3067 <- nc %>% st_transform(crs = st_crs(crs_3067))
sf::st_write(obj = nc_3067, dsn = geopkg_file, layer = "YKRVyohykkeet2017")

#' # keskustatkaupanalueet

#+
dir_create("~/local_data/lahiohanke_paikkatietoaineistot/raw_data/syke/keskustatkaupanalueet")
download.file(url = "https://wwwd3.ymparisto.fi/d3/gis_data/spesific/keskustatkaupanalueet.zip",
              "~/local_data/lahiohanke_paikkatietoaineistot/raw_data/syke/keskustatkaupanalueet/file.zip", mode = "wb")
unzip("~/local_data/lahiohanke_paikkatietoaineistot/raw_data/syke/keskustatkaupanalueet/file.zip", 
      exdir = "~/local_data/lahiohanke_paikkatietoaineistot/raw_data/syke/keskustatkaupanalueet/")

nc <- st_read("~/local_data/lahiohanke_paikkatietoaineistot/raw_data/syke/keskustatkaupanalueet/KaupanAlueet.shp")
nc_3067 <- nc %>% st_transform(crs = st_crs(crs_3067))
sf::st_write(obj = nc_3067, dsn = geopkg_file, layer = "KaupanAlueet")

nc <- st_read("~/local_data/lahiohanke_paikkatietoaineistot/raw_data/syke/keskustatkaupanalueet/KeskustaAlueet.shp")
nc_3067 <- nc %>% st_transform(crs = st_crs(crs_3067))
sf::st_write(obj = nc_3067, dsn = geopkg_file, layer = "KeskustaAlueet")

# KaupanAlueet <- st_intersection(st_make_valid(nc),muni2)

