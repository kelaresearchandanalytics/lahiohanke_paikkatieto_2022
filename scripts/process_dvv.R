
source("./scripts/global.R")
fldr <- "./datasets/"
fs::dir_create(fldr)

geopkg_file <- "./datasets/dvv_3067.gpkg"
file.remove(geopkg_file)


# *****************************************************************************************************
# *****************************************************************************************************
## Rakennusten osoitetiedot ja äänestysalue - koko Suomi
### https://www.avoindata.fi/data/fi/dataset/postcodes

# tmpfile <- "~/local_data/lahiohanke_paikkatietoaineistot/raw_data/finland_addresses_2021-08-16-json.7z"
tmpdir <- "~/local_data/lahiohanke_paikkatietoaineistot/raw_data"
dir_create(tmpdir)
# Ladataan paketti
download.file("https://www.avoindata.fi/data/dataset/941b70c8-3bd4-4b4e-a8fb-0ae29d2647a1/resource/3c277957-9b25-403d-b160-b61fdb47002f/download/finland_addresses_2021-08-16-json.7z",
              destfile = "~/local_data/lahiohanke_paikkatietoaineistot/raw_data/finland_addresses_2021-08-16-json.7z")
# Puretaan paketti
setwd("~/local_data/lahiohanke_paikkatietoaineistot/raw_data")
system('7z x finland_addresses_2021-08-16-json.7z')
setwd(rstudioapi::getActiveProject())

# Listataan paketin tiedostot
dir_info("~/local_data/lahiohanke_paikkatietoaineistot/raw_data/", recurse = TRUE) %>%
  dplyr::select(path,size,modification_time) %>%
  dplyr::arrange(size) -> flies
print(flies, n = 30)

d <- read_csv("~/local_data/lahiohanke_paikkatietoaineistot/raw_data/data/Finland_addresses_2021-08-16.csv",
              col_names = TRUE)

rak_sf <- st_as_sf(d,
                   coords = c("longitude_wgs84",
                              "latitude_wgs84"),
                   crs = "+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs"
                   # crs = "+proj=longlat +datum=WGS84"
                   # crs = "ESPG:4326"
                   # crs = "ESPG:3067"
)

# rikastetaan dataa rakennusten käyttötiedoilla
kayttotark <- read_csv2(file = "https://www.stat.fi/fi/luokitukset/rakennus/rakennus_1_20180712/export/", show_col_types = FALSE)
names(kayttotark) <- as.character(kayttotark[1,])
kayttotark <- kayttotark[-1, ] %>%
  dplyr::mutate(code = as.integer(gsub("'", "", code)),
                level = as.integer(level))
rak_sf2 <- left_join(rak_sf,kayttotark,
                     by = c("building_use" = "code"))

rak_sf2_3067 <- st_transform(rak_sf2, crs = crs_3067)
sf::st_write(obj = rak_sf2_3067, dsn = geopkg_file, layer = "rakennukset_20210816_english")

# *****************************************************************************************************
# *****************************************************************************************************
## Rakennusten osoitetiedot ja äänestysalue - koko Suomi
### https://www.avoindata.fi/data/fi/dataset/rakennusten-osoitetiedot-koko-suomi
source("./scripts/global.R")
# Ladataan paketti
download.file("https://www.avoindata.fi/data/dataset/cf9208dc-63a9-44a2-9312-bbd2c3952596/resource/ae13f168-e835-4412-8661-355ea6c4c468/download/suomi_osoitteet_2021-08-16.7z",
              destfile = "~/local_data/lahiohanke_paikkatietoaineistot/raw_data/suomi_osoitteet_2021-08-16.7z", mode = "wb")
# Puretaan paketti
setwd("~/local_data/lahiohanke_paikkatietoaineistot/raw_data")
system('7z x -aoa suomi_osoitteet_2021-08-16.7z')
setwd(rstudioapi::getActiveProject())

# Listataan paketin tiedostot
dir_info("~/local_data/lahiohanke_paikkatietoaineistot/raw_data/", recurse = TRUE) %>%
  dplyr::select(path,size,modification_time) %>%
  dplyr::arrange(size) -> flies
print(flies, n = 30)


d <- read_csv2("~/local_data/lahiohanke_paikkatietoaineistot/raw_data/Suomi_osoitteet_2021-08-16.OPT",
               col_names = FALSE)

nimet <- c("Rakennustunnus",
           "Sijaintikunta",
           "Maakunta",
           "Käyttötarkoitus",
           "Pohjoiskoordinaatti",
           "Itäkoordinaatti",
           "Osoitenumero",
           "Kadunnimi suomeksi",
           "Kadunnimi ruotsiksi",
           "Katunumero",
           "Postinumero",
           "Äänestysalue",
           "Äänestysalueen nimi suomeksi",
           "Äänestysalueen nimi ruotsiksi",
           "Sijaintikiinteistö",
           "Tietojen poimintapäivä")
nimet <- janitor::make_clean_names(nimet)
names(d) <- nimet

# tiedostonimet pdf:stä
## https://www.avoindata.fi/data/dataset/cf9208dc-63a9-44a2-9312-bbd2c3952596/resource/d2c43faa-5531-4e7c-9f4c-510c39bfa038/download/rakennusten-osoitetiedot-avoin-data-kuvaus-tiedoista-2020-02-14.pdf

rak_sf <- st_as_sf(d,
                   coords = c("itakoordinaatti",
                              "pohjoiskoordinaatti")#,
                   # crs = "+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs"
                   # crs = "+proj=longlat +datum=WGS84"
                   # crs = "ESPG:4326"
                   # crs = "ESPG:3067"
)

st_crs(rak_sf) <- crs_3067
rak_sf_3067 <- st_transform(rak_sf)

kayttotark <- read_csv2(file = "https://www.stat.fi/fi/luokitukset/rakennus/rakennus_1_20180712/export/", show_col_types = FALSE)
names(kayttotark) <- as.character(kayttotark[1,])
kayttotark <- kayttotark[-1, ] %>%
  mutate(code = as.integer(gsub("'", "", code)),
         level = as.integer(level))
rak_sf_3067_2 <- left_join(rak_sf_3067,
                           kayttotark,
                           by = c("kayttotarkoitus" = "code"))

dir_create("./datasets/dvv/")
sf::st_write(obj = rak_sf_3067_2, dsn = geopkg_file, layer = "rakennukset_20210816_suomeksi")
