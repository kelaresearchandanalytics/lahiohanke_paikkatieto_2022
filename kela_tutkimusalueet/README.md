Kelan tutkimusalueet
=====================================

Tästä kansiosta löytyvät Kelan avoimista kansioista löytyvät tutkimusalueiden rajausten paikkatiedot sekä skriptit niiden tekemiseen.

## Datat

- `lahiot_20211119.gpkg`
- `lahiot_20211118.gpkg`


## Datoja vastaavat kartat

Kartat ja muut verkkosisältö löytyvät `./public`-kansiosta ja ne näkyvät rendattuna osoitteessa `https://kelaresearchandanalytics.gitlab.io/lahiohanke_paikkatieto/`

Uusin kartta näkyy suoraan osoitteessa: <https://kelaresearchandanalytics.gitlab.io/lahiohanke_paikkatieto_2022/lahiot_20211119_leaflet_map.html>
